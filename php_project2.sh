#!/bin/bash

# -------------------------------------------------------
# Info:
# 	Miroslav Vidovic
# 	php_project.sh
# 	23.03.2016.-11:52:13
# -------------------------------------------------------
# Description:
#   Script crates a basic PHP directory structure that
#   can be used as a template for new PHP projects.
# Usage:
#
# -------------------------------------------------------
# Script:


# Get user input
input="$@"

# If project name is empty set project name
if [ -z "php_project" ]; then
  project_name="php_project"
fi

# Create the directories in root
create_dirs_in_root(){
  root_dirs=(data temp web test src config vendor bower_components)
  for dir in "${root_dirs[@]}"
  do
    mkdir -p php_project/$dir
  done
}

# Create files in root directory
crate_files_in_root(){
  root_files=(composer.json bower.json .gitignore)
  for file in "${root_files[@]}"
  do
    touch php_project/$file
  done
}

# Create everything in the project root directory
directory_root(){
  create_dirs_in_root
  crate_files_in_root
}

# Create in web
create_dirs_in_web(){
  ph_dirs=(css images js)
  for dir in "${ph_dirs[@]}"
  do
    mkdir -p php_project/web/$dir
  done
}

create_files_in_web(){
  files=(index.php htaccess)
  for file in "${files[@]}"
  do
    touch php_project/web/$file
  done
}

# Create in project_src
create_in_src(){
  res_dirs=(Model Controller View Exception)
  for dir in "${res_dirs[@]}"
  do
    mkdir -p php_project/src/$dir
  done
}

# Use bower to install packages
bower_install_packages(){
  items=(jquery bootstrap)
  cd php_project
  for item in "${items[@]}"
  do
    bower install $item
  done
}

# Check if bower is installed and then run bower or show a warning
run_bower(){
  # Problem
  if bower -q 2>/dev/null; then
      bower_install_packages
  else
    separator
    echo "Warning: Install bower if you want to use additional package download options".
    separator
  fi
}

composer_content(){
  cat << EOF
  {
    "name": "miroslavvidovic/project-name",
    "description": "Project description",
    "authors": [
        {
            "name": "Miroslav Vidovic",
            "email": "miroslav-vidovic@hotmail.com"
        }
    ],
    "require": {

    },
    "autoload": {
        "psr-4": {
            "Project\\":"src/"
        }
    }

}
EOF
}

bower_content(){
  echo
}

gitignore_content(){
  echo
}

htaccess_content(){
 echo
}

separator(){
  printf "%0.s-" {1..80}
  printf "\n"
}

info(){
# Number of created directories
num_dirs=$(ls -lR | grep ^d | wc -l)
# Number of created files
num_files=$(ls -lR | grep ^- | wc -l)
# Used memory
mem=$(du -ch | grep total)
separator
# Remember "EOF" and not EOF when using ascii art
cat << "EOF"
   ___  __ _____                    _         __  _           __
  / _ \/ // / _ \  ___  _______    (_)__ ____/ /_(_)__  ___ _/ /____  ____
 / ___/ _  / ___/ / _ \/ __/ _ \  / / -_) __/ __/ / _ \/ _ `/ __/ _ \/ __/
/_/  /_//_/_/    / .__/_/  \___/_/ /\__/\__/\__/_/_//_/\_,_/\__/\___/_/
                /_/           |___/

EOF
cat<< EOF
New PHP project directory structure has been created:
number of directories: $num_dirs
number of files:  $num_files
disk usage: $mem
EOF
separator
}

help(){
cat<< EOF
PHP projectinator quickly sets up a skeleton for a PHP project.
Options:
  -n : new project
  -h : help
EOF
}

# Solve input with optargs
run(){
  directory_root
  create_dirs_in_web
  create_files_in_web
  create_in_src
  composer_content > php_project/composer.json
  info
}

# Check if the user did not specify any flags when calling the script
# example: bash tpad.sh was called
check_for_empty_input(){
  if [ $# -eq 0 ];
  then
      help
      exit 0
    fi
}

main(){
  while getopts 'nhb' flag; do
    case "${flag}" in
      n)
        run ;;
      h)
        help ;;
    esac
  done

}

# Examples of passing all the input params to functions
check_for_empty_input "$@"
main "$@"


exit 0
